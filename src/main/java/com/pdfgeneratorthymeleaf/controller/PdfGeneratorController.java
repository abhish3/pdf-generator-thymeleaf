package com.pdfgeneratorthymeleaf.controller;

import com.pdfgeneratorthymeleaf.model.Employee;
import com.pdfgeneratorthymeleaf.service.DataMapper;
import com.pdfgeneratorthymeleaf.service.DocumentGenerator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PdfGeneratorController {

    private final SpringTemplateEngine springTemplateEngine;

    private final DataMapper dataMapper;

    private final DocumentGenerator documentGenerator;

    public PdfGeneratorController(SpringTemplateEngine springTemplateEngine, DataMapper dataMapper, DocumentGenerator documentGenerator) {
        this.springTemplateEngine = springTemplateEngine;
        this.dataMapper = dataMapper;
        this.documentGenerator = documentGenerator;
    }

    @PostMapping("/generate-document")
    public String generateDocument(@RequestBody List<Employee> employeeList) {
        String finalHtml = null;
        Context dataContext = dataMapper.setData(employeeList);
        finalHtml = springTemplateEngine.process("EmployeeTemplate", dataContext);
        documentGenerator.htmlToPdf(finalHtml);
        return "Successfully Generated PDF";

    }
}
