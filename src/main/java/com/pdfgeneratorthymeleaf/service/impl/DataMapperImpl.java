package com.pdfgeneratorthymeleaf.service.impl;

import com.pdfgeneratorthymeleaf.model.Employee;
import com.pdfgeneratorthymeleaf.service.DataMapper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataMapperImpl implements DataMapper {

    @Override
    public Context setData(List<Employee> employeeList) {
        Context context = new Context();
        Map<String, Object> data = new HashMap<>();
        data.put("employees", employeeList);
        context.setVariables(data);
        return context;
    }
}
