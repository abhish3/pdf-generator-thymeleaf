package com.pdfgeneratorthymeleaf.service.impl;


import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.pdfgeneratorthymeleaf.service.DocumentGenerator;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

@Service
public class DocumentGeneratorImpl implements DocumentGenerator {
    @Override
    public String htmlToPdf(String processedHtml) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (PdfWriter pdfWriter = new PdfWriter(byteArrayOutputStream);) {
            DefaultFontProvider defaultFontProvider = new DefaultFontProvider(false, true, false);
            ConverterProperties converterProperties = new ConverterProperties();
            converterProperties.setFontProvider(defaultFontProvider);
            HtmlConverter.convertToPdf(processedHtml, pdfWriter, converterProperties);
            String dirPath = System.getProperty("user.home") + "/Desktop/employee.pdf";
            FileOutputStream fos = new FileOutputStream(dirPath);
            byteArrayOutputStream.writeTo(fos);
            byteArrayOutputStream.close();
            byteArrayOutputStream.flush();
        } catch (Exception ex) {
            System.out.println("Exception occurred");
        }

        return null;
    }
}
