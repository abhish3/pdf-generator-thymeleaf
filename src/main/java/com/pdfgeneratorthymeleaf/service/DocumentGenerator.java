package com.pdfgeneratorthymeleaf.service;

public interface DocumentGenerator {

    String htmlToPdf(String processedHtml);
}
