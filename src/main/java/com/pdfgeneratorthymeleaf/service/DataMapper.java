package com.pdfgeneratorthymeleaf.service;

import com.pdfgeneratorthymeleaf.model.Employee;
import org.thymeleaf.context.Context;

import java.util.List;

public interface DataMapper {
    Context setData(List<Employee> employee);
}
